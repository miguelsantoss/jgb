import Register from './register.js';
import { OPCODES } from './opcodes.js';

export default class Cpu {
    constructor() {
        this.registers = new Register();

        this.flagRegister = new Uint8Array(1);
        this.flags = {
            zero: 0b10000000,
            substract: 0b01000000,
            halfCarry: 0b00100000,
            carry: 0b00010000,
        };
        Object.freeze(this.flag);

        this.flagGetSetters();
    }

};

Cpu.OPCODES = [ ...OPCODES ];

Cpu.prototype.flagGetSetters = function () {
    this.set = {};

    Object.defineProperty(this, 'flag', {
        get: () => this.flagRegister[0],
        set: val => this.flagRegister[0] = val
    });

    Object.defineProperty(this, 'z', { get: () => !!(this.flag & this.flags.zero) });
    Object.defineProperty(this.set, 'z',  {get: () =>  { this.flag = this.flag | this.flags.zero; }});

    Object.defineProperty(this, 's', { get: () => !!(this.flag & this.flags.substract) });
    Object.defineProperty(this.set, 's',  {get: () =>  { this.flag = this.flag | this.flags.substract; }});

    Object.defineProperty(this, 'hc', { get: () => !!(this.flag & this.flags.halfCarry) });
    Object.defineProperty(this.set, 'hc',  {get: () =>  { this.flag = this.flag | this.flags.halfCarry; }});

    Object.defineProperty(this, 'c', { get: () => !!(this.flag & this.flags.carry) });
    Object.defineProperty(this.set, 'c',  {get: () =>  { this.flag = this.flag | this.flags.carry; }});
}

Cpu.prototype.run = function(op) {
    Cpu.OPCODES[op].r(this);
}

Cpu.prototype.runOP = function(op, p2) {
    const e = Cpu.OPCODES.find(e => e.op === op);
    let arg = 0;
    switch(e.i) {
    case 'ld':
        const rand = Math.random()*0xFF<<0;
        arg = e.p2 === 0 ? rand : this.registers[e.p2];
        this.registers[e.p1] = arg;
        break;
    case 'ldm':
        if (op === 0xF2) {
            const off = this.registers[e.p2];
            const addr = 0xFF + off;
            arg = this.mmu[addr];
            this.registers[e.p1] = arg;
        }
        else if (op === 0xE2) {
            const off = this.registers[e.p1];
            const addr = 0xFF + off;
            arg = this.registers[e.p1];
            this.mmu[addr] = arg;
        }
        break;
    case 'ldi':
        if (e.op === 0x2A) {
            const addr = this.registers[e.p2];
            this.registers.a = this.mmu[addr];
            this.registers[e.p2]++;
        }
        else if (e.op === 22) {
            const addr = this.registers[e.p1];
            this.mmu[addr] = this.registers.a;
            this.registers[e.p1]++;
        }
        break;
    case 'ldh':
        arg = 0;
        if (e.op === 0xE0) {
            this.mmu[arg] = this.registers[e.p2];
        }
        else if (e.op === 0xF0) {
            this.registers[e.p1] = this.mmu[arg];
        }
        break;
    }
};
