import Cpu from './cpu.js';
import Mmu from './mmu.js';

import bootrom from './bootrom.gb';

const mmu = new Mmu();
const cpu = new Cpu();

console.log(Cpu.OPCODES);

let order = 0;
for(let i = 0; i < Cpu.OPCODES.length; i++) {
    if (i !== Cpu.OPCODES[i].op) {
        order = i;
        break;
    }
}

if (order) console.log('out of order!', (order-1).toString(16));
else console.log('order is good');

cpu.run(0x50);
