export default class Register {
    constructor() {
        this.registers = new Uint8Array(8);
        this.pointers = new Uint16Array(2);

        this.registerGetSetters();
        this.a = 0x0;
        this.b = 0x0;
        this.c = 0x0;
        this.d = 0x0;
        this.e = 0x0;
        this.f = 0x0;
        this.h = 0x0;
        this.l = 0x0;

        this.sp = 0x0;
        this.pc = 0x0;
    }

    getAF() { return (this.a << 8) | this.f; }
    getBC() { return (this.b << 8) | this.c; }
    getDE() { return (this.d << 8) | this.e; }
    getHL() { return (this.h << 8) | this.l; }

    setAF(val) {
        this.a = val & 0xF0;
        this.f = val & 0x0F;
    }

    setBC(val) {
        this.b = val & 0xF0;
        this.c = val & 0x0F;
    }

    setDE(val) {
        this.d = val & 0xF0;
        this.e = val & 0x0F;
    }

    setHL(val) {
        this.h = val & 0xF0;
        this.l = val & 0x0F;
    }
};

Register.prototype.registerGetSetters = function() {
    const a = 0;
    const b = 1;
    const c = 2;
    const d = 3;
    const e = 4;
    const f = 5;
    const h = 6;
    const l = 7;
    const sp = 0;
    const pc = 0;

    Object.defineProperty(this, 'a', {
        get: () => this.registers[a],
        set: val => this.registers[a] = val
    });

    Object.defineProperty(this, 'b', {
        get: () => this.registers[b],
        set: val => this.registers[b] = val
    });
    Object.defineProperty(this, 'c', {
        get: () => this.registers[c],
        set: val => this.registers[c] = val
    });

    Object.defineProperty(this, 'd', {
        get: () => this.registers[d],
        set: val => this.registers[d] = val
    });

    Object.defineProperty(this, 'e', {
        get: () => this.registers[e],
        set: val => this.registers[e] = val
    });

    Object.defineProperty(this, 'f', {
        get: () => this.registers[f],
        set: val => this.registers[f] = val
    });

    Object.defineProperty(this, 'h', {
        get: () => this.registers[h],
        set: val => this.registers[h] = val
    });

    Object.defineProperty(this, 'l', {
        get: () => this.registers[l],
        set: val => this.registers[l] = val
    });

    Object.defineProperty(this, 'sp', {
        get: () => this.pointers[sp],
        set: val => this.pointers[sp] = val
    });

    Object.defineProperty(this, 'pc', {
        get: () => this.pointers[pc],
        set: val => this.pointers[pc] = val
    });

    Object.defineProperty(this, 'af', {
        get: this.getAF,
        set: val => this.setAF(val)
    });

    Object.defineProperty(this, 'bc', {
        get: this.getBC,
        set: val => this.setBC(val)
    });

    Object.defineProperty(this, 'de', {
        get: this.getDE,
        set: val => this.setDE(val)
    });

    Object.defineProperty(this, 'hl', {
        get: this.getHL,
        set: val => this.setHL(val)
    });
};
