export const OPCODES = [
    {
        // nop: do nothing
        // 4 cycles
        op: 0x00,
        r: (cpu) => {
            console.log(cpu);
        }
    },

    {
        // LD BC, nn
        // 12 cycles
        op: 0x01,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // LD BC, nn
        // 12 cycles
        op: 0x02,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x03,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x04,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x05,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // LD B, n
        // 8 cycles
        op: 0x06,
        r: (cpu) => {
            const arg = 0x0;
            cpu.registers.b = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x07,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x08,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x09,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x0a,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x0b,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x0c,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x0d,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 8 cycles
        op: 0x0e,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.c = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x0f,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x10,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x11,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x12,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x13,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x14,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x15,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 0 cycles
        op: 0x16,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.d = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x17,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x18,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x19,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x1a,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x1b,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x1c,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x1d,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 8 cycles
        op: 0x1e,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.e = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x1f,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x20,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x21,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x22,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x23,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x24,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x25,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 8 cycles
        op: 0x26,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.h = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x27,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x28,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x29,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x2a,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x2b,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x2c,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x2d,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 8 cycles
        op: 0x2e,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.l = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x2f,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x30,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x31,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x32,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x33,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x34,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x35,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // LD HL, n
        // 12 cycles
        op: 0x36,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.hl = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x37,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x38,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x39,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x3a,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x3b,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x3c,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x3d,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x3e,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x3f,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // LD B, B
        // 4 cycles
        op: 0x40,
        r: (cpu) => {
            cpu.registers.b = cpu.registers.b;
        }
    },

    {
        // LD B, C
        // 4 cycles
        op: 0x41,
        r: (cpu) => {
            cpu.registers.b = cpu.registers.c;
        }
    },

    {
        // LD B, D
        // 4 cycles
        op: 0x42,
        r: (cpu) => {
            cpu.registers.b = cpu.registers.d;
        }
    },

    {
        // LD B, E
        // 4 cycles
        op: 0x43,
        r: (cpu) => {
            cpu.registers.b = cpu.registers.e;
        }
    },

    {
        // LD B, H
        // 4 cycles
        op: 0x44,
        r: (cpu) => {
            cpu.registers.b = cpu.registers.h;
        }
    },

    {
        // LD B, L
        // 4 cycles
        op: 0x45,
        r: (cpu) => {
            cpu.registers.b = cpu.registers.l;
        }
    },

    {
        // LD B, HL
        // 8 cycles
        op: 0x46,
        r: (cpu) => {
            cpu.registers.b = cpu.registers.hl;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x47,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // LD C, B
        // 4 cycles
        op: 0x48,
        r: (cpu) => {
            cpu.registers.c = cpu.registers.b;
        }
    },

    {
        // LD C, C
        // 4 cycles
        op: 0x49,
        r: (cpu) => {
            cpu.registers.c = cpu.registers.c;
        }
    },

    {
        // LD C, D
        // 4 cycles
        op: 0x4a,
        r: (cpu) => {
            cpu.registers.c = cpu.registers.d;
        }
    },

    {
        // LD C, E
        // 4 cycles
        op: 0x4b,
        r: (cpu) => {
            cpu.registers.c = cpu.registers.e;
        }
    },

    {
        // LD C, H
        // 4 cycles
        op: 0x4c,
        r: (cpu) => {
            cpu.registers.c = cpu.registers.h;
        }
    },

    {
        // LD C, L
        // 4 cycles
        op: 0x4d,
        r: (cpu) => {
            cpu.registers.c = cpu.registers.l;
        }
    },

    {
        // LD C, HL
        // 8 cycles
        op: 0x4e,
        r: (cpu) => {
            cpu.registers.c = cpu.registers.hl;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x4f,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // LD D, B
        // 4 cycles
        op: 0x50,
        r: (cpu) => {
            cpu.registers.d = cpu.registers.b;
        }
    },

    {
        // LD D, C
        // 4 cycles
        op: 0x51,
        r: (cpu) => {
            cpu.registers.d = cpu.registers.c;
        }
    },

    {
        // LD D, D
        // 4 cycles
        op: 0x52,
        r: (cpu) => {
            cpu.registers.d = cpu.registers.d;
        }
    },

    {
        // LD D, E
        // 4 cycles
        op: 0x53,
        r: (cpu) => {
            cpu.registers.d = cpu.registers.e;
        }
    },

    {
        // LD D, H
        // 4 cycles
        op: 0x54,
        r: (cpu) => {
            cpu.registers.d = cpu.registers.h;
        }
    },

    {
        // LD D, L
        // 4 cycles
        op: 0x55,
        r: (cpu) => {
            cpu.registers.d = cpu.registers.l;
        }
    },

    {
        // LD D, HL
        // 8 cycles
        op: 0x56,
        r: (cpu) => {
            cpu.registers.d = cpu.registers.hl;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x57,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // LD E, B
        // 4 cycles
        op: 0x58,
        r: (cpu) => {
            cpu.registers.e = cpu.registers.b;
        }
    },

    {
        // LD E, C
        // 4 cycles
        op: 0x59,
        r: (cpu) => {
            cpu.registers.e = cpu.registers.c;
        }
    },

    {
        // LD E, D
        // 4 cycles
        op: 0x5a,
        r: (cpu) => {
            cpu.registers.e = cpu.registers.d;
        }
    },

    {
        // LD E, E
        // 4 cycles
        op: 0x5b,
        r: (cpu) => {
            cpu.registers.e = cpu.registers.e;
        }
    },

    {
        // LD E, H
        // 4 cycles
        op: 0x5c,
        r: (cpu) => {
            cpu.registers.e = cpu.registers.h;
        }
    },

    {
        // LD E, L
        // 4 cycles
        op: 0x5d,
        r: (cpu) => {
            cpu.registers.e = cpu.registers.l;
        }
    },

    {
        // LD E, HL
        // 8 cycles
        op: 0x5e,
        r: (cpu) => {
            cpu.registers.e = cpu.registers.hl;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x5f,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // LD H, B
        // 4 cycles
        op: 0x60,
        r: (cpu) => {
            cpu.registers.h = cpu.registers.b;
        }
    },

    {
        // LD H, C
        // 4 cycles
        op: 0x61,
        r: (cpu) => {
            cpu.registers.h = cpu.registers.c;
        }
    },

    {
        // LD H, D
        // 4 cycles
        op: 0x62,
        r: (cpu) => {
            cpu.registers.h = cpu.registers.d;
        }
    },

    {
        // LD H, E
        // 4 cycles
        op: 0x63,
        r: (cpu) => {
            cpu.registers.h = cpu.registers.e;
        }
    },

    {
        // LD H, H
        // 4 cycles
        op: 0x64,
        r: (cpu) => {
            cpu.registers.h = cpu.registers.h;
        }
    },

    {
        // LD H, L
        // 4 cycles
        op: 0x65,
        r: (cpu) => {
            cpu.registers.h = cpu.registers.l;
        }
    },

    {
        // LD H, HL
        // 8 cycles
        op: 0x66,
        r: (cpu) => {
            cpu.registers.h = cpu.registers.hl;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x67,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // LD L, B
        // 4 cycles
        op: 0x68,
        r: (cpu) => {
            cpu.registers.l = cpu.registers.b;
        }
    },

    {
        // LD L, C
        // 4 cycles
        op: 0x69,
        r: (cpu) => {
            cpu.registers.l = cpu.registers.c;
        }
    },

    {
        // LD L, D
        // 4 cycles
        op: 0x6a,
        r: (cpu) => {
            cpu.registers.l = cpu.registers.d;
        }
    },

    {
        // LD L, E
        // 4 cycles
        op: 0x6b,
        r: (cpu) => {
            cpu.registers.l = cpu.registers.e;
        }
    },

    {
        // LD L, H
        // 4 cycles
        op: 0x6c,
        r: (cpu) => {
            cpu.registers.l = cpu.registers.h;
        }
    },

    {
        // LD L, L
        // 4 cycles
        op: 0x6d,
        r: (cpu) => {
            cpu.registers.l = cpu.registers.l;
        }
    },

    {
        // LD L, HL
        // 8 cycles
        op: 0x6e,
        r: (cpu) => {
            cpu.registers.l = cpu.registers.hl;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x6f,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // LD HL, B
        // 8 cycles
        op: 0x70,
        r: (cpu) => {
            cpu.registers.hl = cpu.registers.b;
        }
    },

    {
        // LD HL, C
        // 8 cycles
        op: 0x71,
        r: (cpu) => {
            cpu.registers.hl = cpu.registers.c;
        }
    },

    {
        // LD HL, D
        // 8 cycles
        op: 0x72,
        r: (cpu) => {
            cpu.registers.hl = cpu.registers.d;
        }
    },

    {
        // LD HL, E
        // 8 cycles
        op: 0x73,
        r: (cpu) => {
            cpu.registers.hl = cpu.registers.e;
        }
    },

    {
        // LD HL, H
        // 8 cycles
        op: 0x74,
        r: (cpu) => {
            cpu.registers.hl = cpu.registers.h;
        }
    },

    {
        // LD HL, L
        // 8 cycles
        op: 0x75,
        r: (cpu) => {
            cpu.registers.hl = cpu.registers.l;
        }
    },

    {
        // TODO
        // LD HL, HL
        // 8 cycles
        op: 0x76,
        r: (cpu) => {
            cpu.registers.hl = cpu.registers.hl;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x77,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // LD A, B
        // 4 cycles
        op: 0x78,
        r: (cpu) => {
            cpu.registers.a = cpu.registers.b;
        }
    },

    {
        // LD A, C
        // 4 cycles
        op: 0x79,
        r: (cpu) => {
            cpu.registers.a = cpu.registers.c;
        }
    },

    {
        // LD A, D
        // 4 cycles
        op: 0x7a,
        r: (cpu) => {
            cpu.registers.a = cpu.registers.d;
        }
    },

    {
        // LD A, E
        // 4 cycles
        op: 0x7b,
        r: (cpu) => {
            cpu.registers.a = cpu.registers.e;
        }
    },

    {
        // LD A, H
        // 4 cycles
        op: 0x7c,
        r: (cpu) => {
            cpu.registers.a = cpu.registers.h;
        }
    },

    {
        // LD A, L
        // 4 cycles
        op: 0x7d,
        r: (cpu) => {
            cpu.registers.a = cpu.registers.l;
        }
    },

    {
        // LD A, HL
        // 8 cycles
        op: 0x7e,
        r: (cpu) => {
            cpu.registers.a = cpu.registers.hl;
        }
    },

    {
        // LD A, A
        // 4 cycles
        op: 0x7f,
        r: (cpu) => {
            cpu.registers.a = cpu.registers.a;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x80,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x81,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x82,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x83,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x84,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x85,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x86,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x87,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x88,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x89,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x8a,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x8b,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x8c,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x8d,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x8e,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x8f,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x90,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x91,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x92,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x93,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x94,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x95,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x96,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x97,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x98,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x99,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x9a,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x9b,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x9c,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x9d,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x9e,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0x9f,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xa0,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xa1,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xa2,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xa3,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xa4,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xa5,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xa6,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xa7,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xa8,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xa9,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xaa,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xab,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xac,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xad,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xae,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xaf,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xb0,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xb1,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xb2,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xb3,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xb4,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xb5,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xb6,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xb7,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xb8,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xb9,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xba,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xbb,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xbc,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xbd,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xbe,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xbf,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xc0,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xc1,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xc2,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xc3,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xc4,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xc5,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xc6,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xc7,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xc8,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xc9,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xca,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xcb,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xcc,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xcd,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xce,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xcf,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xd0,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xd1,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xd2,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xd3,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xd4,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xd5,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xd6,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xd7,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xd8,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xd9,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xda,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xdb,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xdc,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xdd,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xde,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xdf,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xe0,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xe1,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xe2,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xe3,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xe4,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xe5,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xe6,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xe7,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xe8,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xe9,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xea,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xeb,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xec,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xed,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xee,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xef,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xf0,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xf1,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xf2,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xf3,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xf4,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xf5,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xf6,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xf7,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xf8,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xf9,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xfa,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xfb,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xfc,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xfd,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xfe,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },

    {
        // TODO
        // LD BC, nn
        // 12 cycles
        op: 0xff,
        r: (cpu) => {
            // TODO arg
            const arg = 0x0;
            cpu.registers.bc = arg;
        }
    },
];
